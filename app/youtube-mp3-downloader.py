#!/usr/bin/python3
from __future__ import unicode_literals

import sys
import os
import subprocess
import glob
import time
import traceback
import youtube_dl

from src.utils import *


# ---[ Constants ]------------------------------------------------------------

YTDL_HOME = os.environ['YTDL_HOME']
DROP_PATH = f"{YTDL_HOME}/drop"
OUTPUT_PATH = f"{YTDL_HOME}/download"

YT_URL_HEADERS = [
    'URL=http://www.youtube.com/watch?v=',
    'URL[$e]=http://www.youtube.com/watch?v=',
    'URL=http://youtube.com/watch?v=',
    'URL[$e]=http://youtube.com/watch?v=',

    'URL=https://www.youtube.com/watch?v=',
    'URL[$e]=https://www.youtube.com/watch?v=',
    'URL=https://youtube.com/watch?v=',
    'URL[$e]=https://youtube.com/watch?v=',
]

# ---[ Basic methods ]------------------------------------------------------------

def youTubeDownload(videoId):
    showMsg('Downloading mp3 track from: ' + videoId)

    ydl_opts = {
        'outtmpl': '{:}/%(title)s.%(ext)s'.format(OUTPUT_PATH),
        'format': 'bestaudio/best',
        'postprocessors': [{
            'key': 'FFmpegExtractAudio',
            'preferredcodec': 'mp3',
            'preferredquality': '192',
        }],
    }

    with youtube_dl.YoutubeDL(ydl_opts) as ydl:
        try:
            ydl.download(['http://www.youtube.com/watch?v=' + videoId])
        except:
            showErrorMsg('Downloading video')
            pass

def checkDropArea():
    for fileName in glob.glob(DROP_PATH + '/*'):

        shortFileName = fileName[len(DROP_PATH + '/'):]
        # displayStr = shortFileName.encode(sys.stdout.encoding, errors='replace')
        displayStr = str(shortFileName.encode('UTF-8', errors='replace'), 'utf-8')
        showTitle(displayStr)

        f = open(fileName)
        found = False
        for line in f:
            for hdr in YT_URL_HEADERS:
                line = line.strip("\n")
                if line[:len(hdr)] == hdr:
                    found = True
                    videoId = line[len(hdr):]
                    videoId = videoId.split('&')[0]
                    youTubeDownload(videoId)
                    showMsg('Download finished')

                    break

        if not found:
            showErrorMsg('No link to a youtube video has been found')

        f.close()
        os.remove(fileName)


# ---[ Main code section ]-----------------------------------------------------------------------------------

def main():
    showTitle('youTube mp3 downloader')

    showData("Drop path", DROP_PATH)
    showData("Output path", OUTPUT_PATH)

    showMsg('Waiting for links...')
    while True:
        try:
            checkDropArea()
        except:
            showErrorMsg('Processing link')
        time.sleep(0.05)

# ---[ Exception closure ]------------------------------------------------------------

if __name__ == "__main__":
    try:
        main()

    except KeyboardInterrupt:
        print("*")
        print("* CTRL+C pressed, closing...")
        print("*")

    except:
        print("*")
        print("* EXCEPTION catched:")
        print("*")
        print("*", sys.exc_info()[0])
        traceback.print_exc(file=sys.stdout)
        print("*")
        print("* Closing...")
        print("*")

    finally:
        # freeResources()
        pass
