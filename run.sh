#!/bin/bash

# From: https://stackoverflow.com/questions/59895/get-the-source-directory-of-a-bash-script-from-within-the-script-itself
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
# ---------------------------------------------------------

source $DIR/.env

docker run \
  -it \
  --rm \
  --env-file $DIR/.env \
  --volume $OUTPUT_FOLDER/download:/youtube-mp3-downloader/download \
  --volume $OUTPUT_FOLDER/drop:/youtube-mp3-downloader/drop \
  youtube-mp3-downloader \
  $1 $2 $3 $4 $5 $6 $7 $8 $9
