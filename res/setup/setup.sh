#!/bin/bash
echo "youtube mp3 downloader setup"
echo

# Check if we're running from the repo folder
if [ ! -f "$(pwd)/app/youtube-mp3-downloader.py" ]
then
    echo "Please run me from the repo folder"
    exit 1
fi

# Folders
DEFAULT_OUTFOLDER=$HOME/.youtube-mp3-downloader
OUTFOLDER=$DEFAULT_OUTFOLDER
REPOFOLDER=$(pwd)
USE_DEFAULT_OUTFOLDER="yes"

# Check parameters
usage()
{
  echo "Usage: $0 [DOWNLOAD_FOLDER] [options]"
  echo
  echo "If DOWNLOAD_FOLDER is not provided, \"$DEFAULT_OUTFOLDER\" will be used"
  echo
  echo "Options:"
  echo "  -h, --help: show this help text"
}

while [ "$1" != "" ]; do
  case $1 in
    -h | --help )
      usage
      exit 1
      ;;
    * )
      OUTFOLDER=$1
      USE_DEFAULT_OUTFOLDER=""
  esac
  shift
done

if [ "$USE_DEFAULT_OUTFOLDER" ]
then
  echo "Using default output folder: $OUTFOLDER"
fi

# Create output folders
echo "Creating output folders..."
echo "- $OUTFOLDER/drop"
mkdir --parents $OUTFOLDER/drop
echo "- $OUTFOLDER/download"
mkdir --parents $OUTFOLDER/download

# .env
echo "Creating new .env file..."
echo "" > $REPOFOLDER/.env
echo "OUTPUT_FOLDER=$OUTFOLDER" >> $REPOFOLDER/.env
echo "YTDL_HOME=/youtube-mp3-downloader" >> $REPOFOLDER/.env

# Build docker container
echo "Building container..."
echo
$REPOFOLDER/dev/build.sh
echo

# Create activity icon
echo "Creating activity icon..."

replaceTags()
{
  sed -i "s/__USERNAME__/$USER/g" $1
  sed -i "s/__OUTPATH__/$ESCAPED_OUTFOLDER/g" $1
  sed -i "s/__REPOPATH__/$ESCAPED_REPOFOLDER/g" $1
}

ESCAPED_REPOFOLDER=${REPOFOLDER//\//\\/}
ESCAPED_OUTFOLDER=${OUTFOLDER//\//\\/}
SHELLSCRIPT=$HOME/.local/share/applications/ytmp3dl.sh
DESKTOPFILE=$HOME/.local/share/applications/ytmp3dl.desktop

cp --force $REPOFOLDER/res/setup/ytmp3dl.sh.example $SHELLSCRIPT
replaceTags $SHELLSCRIPT

cp --force $REPOFOLDER/res/setup/ytmp3dl.desktop.example $DESKTOPFILE
replaceTags $DESKTOPFILE

echo
echo "Finished"
