# youtube-mp3-downloader

Download the audio stream of a youtube video to an mp3 file

## usage

* Execute the `run.sh` script from the project folder
* Drop youtube video links in your `youtube-music\drop` folder
* Downloaded files will be in your `youtube-music\download` folder

## setup

### prerequisites

* This has been tested/used under [Ubuntu 18.04 LTS](http://releases.ubuntu.com/18.04/)
* You need [Docker](https://www.docker.com/) to make this thing run
* To avoid needing `sudo` to run Docker, make sure you put yourself into the `docker` group
```
$ sudo usermod -aG docker ${USER}
```

### setup procedure

* Clone the repo
* Create a folder somewhere in your filesystem for links and downloaded mp3 files (we'll use `/home/someuser/youtube-music` in this example)
* Run the setup script from the repo folder indicating the new folder name
```
$ res/setup/setup.sh /home/someuser/youtube-music
```

### update

To update to a newer version, first do a `git pull` from the repo folder
```
$ git pull
```

Then rebuild the container
```
$ dev/build.sh
```