#!/bin/bash
source .env

docker run \
  -it \
  --rm \
  --env-file .env \
  --volume $OUTPUT_FOLDER/download:/youtube-mp3-downloader/download \
  --volume $OUTPUT_FOLDER/drop:/youtube-mp3-downloader/drop \
  --volume $(pwd)/app:/youtube-mp3-downloader/app \
  youtube-mp3-downloader \
  $1 $2 $3 $4 $5 $6 $7 $8 $9


