FROM python:3-alpine

RUN apk add --no-cache ffmpeg && \
    pip3 install --upgrade pip && \
    pip3 install youtube-dl

ENV YTDL_HOME=/youtube-mp3-downloader

COPY app $YTDL_HOME/app

WORKDIR $YTDL_HOME/app
CMD ["python3", "-u", "youtube-mp3-downloader.py"]
